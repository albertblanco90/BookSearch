﻿namespace BookSearch.ViewModels
{
    public class MainViewModel
    {
        #region ViewModels

        public BookViewModel BookSearch
        {
            get;
            set;
        }

        public DescViewModel Desc
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this; // Singlenton
            this.BookSearch = new BookViewModel();
        }
        #endregion

        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion
    }
}
