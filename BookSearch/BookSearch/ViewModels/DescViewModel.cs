﻿namespace BookSearch.ViewModels
{
    using Views;
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Services;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class DescViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private ObservableCollection<Book> books;
        private BookResponse bookResponse;
        private bool isRunning;
        private string textSearch;
        private bool isEnabled;
        #endregion

        #region Propperties

        public bool IsRunning
        {
            get { return this.isRunning; }
            set { SetValue(ref this.isRunning, value); }
        }

        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }
        }

        public string TextSearch
        {
            get { return this.textSearch; }
            set { SetValue(ref this.textSearch, value); }
        }

        public ObservableCollection<Book> Books
        {
            get { return this.books; }
            set { SetValue(ref this.books, value); }

        }
        #endregion

        #region Constructor
        public DescViewModel()
        {
            this.apiService = new ApiService();
            this.IsEnabled = true;

        }
        #endregion

        #region Methods
        private async void LoadBook()
        {

            this.IsRunning = true;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRunning = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Accept");
                return;
            }

            var apiBible = Application.Current.Resources["API"].ToString();
            var response = await this.apiService.Get<BookResponse>(
                apiBible,
                "search/",
                 textSearch);



            if (!response.IsSuccess)
            {
                this.IsRunning = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Accept");
                return;
            }


            this.IsEnabled = true;
            this.IsRunning = false;

            this.bookResponse = (BookResponse)response.Result;

            this.Books = new ObservableCollection<Book>(ToResult());
        }

        private IEnumerable<Book> ToResult()
        {
            return this.bookResponse.Books.Select(b => new Book
            {
                Image = b.Image,
                Description = b.Description,

            });
        }


        #endregion
    }
}
