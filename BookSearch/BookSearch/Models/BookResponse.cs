﻿namespace BookSearch.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class BookResponse
    {
        [JsonProperty("Error")]
        public string Error { get; set; }

        [JsonProperty("Time")]
        public double Time { get; set; }

        [JsonProperty("Total")]
        public string Total { get; set; }

        [JsonProperty("Page")]
        public long Page { get; set; }

        [JsonProperty("Books")]
        public List<Book> Books { get; set; }
    }
}
