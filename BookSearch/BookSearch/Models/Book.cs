﻿namespace BookSearch.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class Book
    {
        [JsonProperty("ID")]
        public long Id { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Image")]
        public string Image { get; set; }

        [JsonProperty("isbn")]
        public string Isbn { get; set; }

        [JsonProperty("SubTitle", NullValueHandling = NullValueHandling.Ignore)]
        public string SubTitle { get; set; }

        [JsonProperty("Books")]
        public List<Book> Books { get; set; }
    }
}
