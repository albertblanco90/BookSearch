using Xamarin.Forms.Xaml;
[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace BookSearch
{
    using BookSearch.ViewModels;
    using BookSearch.Views;
    using Xamarin.Forms;


    public partial class App : Application
	{
        #region Properties

        public static NavigationPage Navigator
        {
            get;
            internal set;
        }

        #endregion
        public App ()
		{
			InitializeComponent();

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.BookSearch = new BookViewModel();
            Application.Current.MainPage = new NavigationPage(new BookPage());
            
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
